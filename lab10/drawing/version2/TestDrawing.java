package drawing.version2;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;
import shapes.Shape;

public class TestDrawing {

	public static void main(String[] args) {

		Drawing drawing = new Drawing();

		drawing.addShape(new Shape(5));

		System.out.println("Total area = " + drawing.calculateTotalArea());
	}

}
