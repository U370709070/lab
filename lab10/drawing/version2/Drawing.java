package drawing.version2;

import java.util.ArrayList;
//import javax.lang.model.util.ElementScanner6;
import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;
import shapes.Shape;

public class Drawing {

	// private ArrayList<Shape> shapes = new ArrayList<Shape>();
	ArrayList<Shape> shapes = new ArrayList<Shape>();

	public double calculateTotalArea() {
		double totalArea = 0;
		
		for (Object shape : shapes) {
			if (shape instanceof Circle) {
				Circle c = (Circle) shape;
				totalArea += c.area();
			} else if (shape instanceof Square) {
				Square s = (Square) shape;
				totalArea += s.area();
			} else if(shape instanceof Rectangle) {
				Rectangle r = (Rectangle) shape;
				totalArea += r.area();
			}
		}

		return totalArea;
	}

	public void addShape(Shape s) {
		shapes.add(s);
	}

}
