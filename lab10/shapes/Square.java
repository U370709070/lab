package shapes;

public class Square extends Shape{
    private double side;

    public Square(double a) {
    	super();
        this.side = a;
    }

    public double area() {
        return this.side * this.side;
    }

}
