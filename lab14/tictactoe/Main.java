package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InvalidMoveException {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();

		System.out.println(board);
		while (!board.isEnded()) {
			int row = 0, col = 0;
			int player = board.getCurrentPlayer();

			boolean var = false;

			do {
				try {

					System.out.print("Player " + player + " enter row number:");
					row = reader.nextInt();
					var = false;
				} catch (InputMismatchException e) {
					System.out.println("Please enter int value for row");
					var = true;
					reader.nextLine();
				}

			} while (var);
			
			var = false;
			
			do {
				try {

					System.out.print("Player " + player + " enter column number:");
					col = reader.nextInt();
					var = false;
				} catch (InputMismatchException e) {
					System.out.println("Please enter int value for column");
					var = true;
 					reader.nextLine();
				}

			} while (var);

			board.move(row, col);
			System.out.println(board);
		}
		reader.close();

	}

}
