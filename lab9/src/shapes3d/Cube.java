package shapes3d;

import shapes2d.Square;

public class Cube extends Square{
	public Cube() {
		super(1.0);
	}
	public Cube(double a) {
		super(a);
	}
	public double getSide() {
		return super.getSide();
	}
	public void setSide(double a) {
		super.setSide(a);
	}
	public double area() {
		return (super.area()*6);
	}
	public double volume() {
		return (super.area())*super.getSide();
	}
	public String toString() {
		return super.toString();
	}


}
