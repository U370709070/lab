package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
	private double height;
	public Cylinder() {
		super();
		this.height=1.0;
	}
	public Cylinder(double r,double h) {
		super(r);
		this.height=h;
	}
	/*public double getRadius() {
		return super.getRadius();
	}
	public void setRadius(double a) {
		super.setRadius(a);
	}
	public double getHeigh()
	{
		return this.height;
	}
	public void setHeight(double h) {
		this.height=h;
	}*/
	public double area() {
		return (super.area()*2)*((this.height*2)*(2*3.14*super.getRadius()));
	}
	public double volume() {
		return super.area()*this.height;	
	}
	
}
