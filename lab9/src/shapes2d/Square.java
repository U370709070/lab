package shapes2d;

public class Square {
	private double side;
	public Square() {
		this.side=1.0;
	}
	public Square(double a) {
		this.side=a;
	}
	public double getSide() {
	    return this.side;
	}
	public void setSide(double a) {
		this.side=a;
	}
	public double area() {
		return (this.side*this.side);
	}
	public String toString() {
		return "Side="+ this.side;
	}

}
