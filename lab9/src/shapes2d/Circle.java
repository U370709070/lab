package shapes2d;

public class Circle {
	private double radius;
	public Circle() {
		this.radius=1.0;
	}
	public Circle(double a) {
		this.radius=a;
	}
	public double getRadius() {
		return this.radius;
	}
	public void setRadius(double a) {
		this.radius=a;
	}
	public double area() {
		return (this.radius*this.radius)*3.14;
	}
	public String toString() {
		return "Radius="+this.radius;
	}

}
