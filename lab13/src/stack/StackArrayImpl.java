package stack;

import java.util.ArrayList;
import stack.StackImpl;

public class StackArrayImpl {
	
	ArrayList<Object> stack=new ArrayList<Object>();
	
	public StackArrayImpl(Object a) {
		stack.add((StackImpl) a);
		
	}
	public StackArrayImpl() {
		// TODO Auto-generated constructor stub
	}
	public void push(Object a) {
		stack.add( a);
	}
	
	public Object pop() {
		if(stack.size()==1) {
			Object a;
			a=stack.get(0);
			stack.remove(0);
			return a;
			
			
		}
		
		int i =stack.size()-1; 
		Object a;
		a=stack.get(i);
		stack.remove(i);
		return a;
	}
	public boolean empty() {
		if(stack.size()==0)
			return true;
		return false;
	}
	
	
	

}
