package stack;

import stack.Stack;

public class StackItem {
	Stack item;

	public StackItem(Object a) {
		this.item = (Stack) a;
	}

	public void setItem(Object a) {
		this.item = (Stack) a;
	}

	public Object getItem() {
		return this.item;
	}

}
