package demo;


import stack.StackArrayImpl;

public class StackDemo {

	public static void main(String[] args) {
		StackArrayImpl stack = new StackArrayImpl();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		while (stack.empty()==false) {
			System.out.println(stack.pop());
		}
	}

}
