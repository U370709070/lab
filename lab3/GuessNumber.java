

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int guess;
		
		
		
		//Read the user input
		int var=1;
		while (var==1) {
			System.out.println("Hi! I'm thinking of a number between 0 and 99.");
			System.out.print("Can you guess it: ");
			guess = reader.nextInt();
		if (guess==number) {
			System.out.println("Congratulations!");
			var=0;
			
		}
		else if(guess < number && guess <101) {
			System.out.println("Your guess is less than mine");
			var=1;
		}
		else if (guess > number && guess<101) {
			System.out.println("Your guess is greater than mine");
			var=1;
		}
		else {	
			System.out.println("Finish... The number was="+number);
			var=0;
		}
		
	}
		reader.close(); //Close the resource before exiting
	
	}
}
